namespace sporMerkezi1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialIgnoreChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ders",
                c => new
                    {
                        DersID = c.Int(nullable: false),
                        DersAdi = c.String(),
                        DersTarihi = c.DateTime(nullable: false),
                        DersSaati = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DersID);
            
            CreateTable(
                "dbo.GrupKayit",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        grupKayitID = c.Int(nullable: false),
                        DersID = c.Int(nullable: false),
                        UyeKayit_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Ders", t => t.DersID, cascadeDelete: true)
                .ForeignKey("dbo.UyeKayit", t => t.UyeKayit_ID)
                .Index(t => t.DersID)
                .Index(t => t.UyeKayit_ID);
            
            CreateTable(
                "dbo.UyeKayit",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AdSoyad = c.String(nullable: false),
                        TcNo = c.String(nullable: false),
                        DogumTarihi = c.DateTime(nullable: false),
                        Telefon = c.String(nullable: false),
                        EPosta = c.String(nullable: false),
                        KullaniciAdi = c.String(),
                        Sifre = c.String(),
                        YeniSifre = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Giris",
                c => new
                    {
                        girisId = c.Int(nullable: false, identity: true),
                        KullaniciAdi = c.String(nullable: false),
                        Sifre = c.String(nullable: false),
                        HatırlaBeni = c.Boolean(nullable: false),
                        UyeKayit_ID = c.Int(),
                    })
                .PrimaryKey(t => t.girisId)
                .ForeignKey("dbo.UyeKayit", t => t.UyeKayit_ID)
                .Index(t => t.UyeKayit_ID);
            
            CreateTable(
                "dbo.Yorum",
                c => new
                    {
                        YorumID = c.Int(nullable: false, identity: true),
                        Tarih = c.DateTime(nullable: false),
                        Ekle = c.String(),
                        UyeKayit_ID = c.Int(),
                    })
                .PrimaryKey(t => t.YorumID)
                .ForeignKey("dbo.UyeKayit", t => t.UyeKayit_ID)
                .Index(t => t.UyeKayit_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Yorum", "UyeKayit_ID", "dbo.UyeKayit");
            DropForeignKey("dbo.GrupKayit", "UyeKayit_ID", "dbo.UyeKayit");
            DropForeignKey("dbo.Giris", "UyeKayit_ID", "dbo.UyeKayit");
            DropForeignKey("dbo.GrupKayit", "DersID", "dbo.Ders");
            DropIndex("dbo.Yorum", new[] { "UyeKayit_ID" });
            DropIndex("dbo.Giris", new[] { "UyeKayit_ID" });
            DropIndex("dbo.GrupKayit", new[] { "UyeKayit_ID" });
            DropIndex("dbo.GrupKayit", new[] { "DersID" });
            DropTable("dbo.Yorum");
            DropTable("dbo.Giris");
            DropTable("dbo.UyeKayit");
            DropTable("dbo.GrupKayit");
            DropTable("dbo.Ders");
        }
    }
}
