﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using sporMerkezi1.Areas.Admin.Models;

namespace sporMerkezi1.Models
{
    public class UyeKayit
    {
        

        public int ID { get; set; }

        [Required (ErrorMessage ="Lütfen Ad Soyad bilginizi giriniz.")]
        public string AdSoyad { get; set; }
        [Required(ErrorMessage = "Lütfen T.C Kimlik No bilginizi giriniz.")]
        public string TcNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Doğum Tarihi")]
        public DateTime DogumTarihi { get; set; }
        [Required(ErrorMessage = "Lütfen Telefon bilginizi giriniz.")]
        public string Telefon { get; set; }
        [Required(ErrorMessage = "Lütfen E-mail bilginizi giriniz.")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Lütfen geçerli bir E-mail bilgisi giriniz.")]
        public string EPosta { get; set; }
        public string  KullaniciAdi   { get; set; }
        public string Sifre { get; set; }
        public string YeniSifre { get; set; }

        public virtual ICollection<GrupKayit> GrupKayit { get; set; }

        public virtual ICollection<Yorum> Yorum { get; set; }

        public virtual ICollection<Giris> Giris { get; set; }

    }
}