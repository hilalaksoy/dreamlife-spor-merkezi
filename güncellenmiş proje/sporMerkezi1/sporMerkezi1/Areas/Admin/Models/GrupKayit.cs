﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sporMerkezi1.Models
{
    public class GrupKayit
    {
        public int grupKayitID { get; set; }
        public int DersID { get; set; }
        public int ID { get; set; }
       
       
        public virtual Ders Ders  { get; set; }
        public virtual UyeKayit UyeKayit { get; set; }

    }
}