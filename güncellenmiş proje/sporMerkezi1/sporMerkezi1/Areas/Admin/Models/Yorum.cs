﻿using sporMerkezi1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sporMerkezi1.Areas.Admin.Models
{
    public class Yorum
    {
        public int YorumID { get; set; }

        public DateTime Tarih { get; set; }

        public string Ekle { get; set; }

        public virtual UyeKayit UyeKayit { get; set; }
    }
}