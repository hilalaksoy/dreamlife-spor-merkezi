﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sporMerkezi1.Models
{
    public class Ders
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DersID { get; set; }

        public string DersAdi { get; set; }

        [DisplayFormat(DataFormatString = "<%=day(date)%>", ApplyFormatInEditMode = true)]
        public DateTime DersTarihi { get; set; }
        [DisplayFormat(DataFormatString = "<%=hour(time)%>", ApplyFormatInEditMode = true)]
        public DateTime DersSaati { get; set; }

        public virtual ICollection<GrupKayit> GrupKayitlar { get; set; }

    }
}