﻿using sporMerkezi1.Areas.Admin.Models;
using sporMerkezi1.DAL;
using sporMerkezi1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace sporMerkezi1.Areas.Admin.Controllers
{
    public class YorumController : Controller
    {
       
        [HttpGet]
        public ActionResult yorumIndex()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        // GET: Admin/Yorum
        public ActionResult yorumIndex(Yorum Yorums)
        {
            if (ModelState.IsValid)
            {
                var yorum = new Yorum() { Tarih = Yorums.Tarih, Ekle = Yorums.Ekle, };
                var dbContext = new sporContext();
                dbContext.Yorumlar.Add(yorum);
                dbContext.SaveChanges();             
                return RedirectToAction("Index","../Home");
            }

            else
            {
                return View();
            }
        }

    }
}