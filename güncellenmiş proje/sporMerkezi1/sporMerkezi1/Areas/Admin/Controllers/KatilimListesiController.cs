﻿using sporMerkezi1.DAL;
using sporMerkezi1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace sporMerkezi1.Areas.Admin.Controllers
{
    public class KatilimListesiController : Controller
    {

        //public ActionResult kListe()
        //{
           
            //    return View(Tuple.Create<UyeKayit, Ders>(new UyeKayit(), new Ders()));
            //    return View();
            //}
            //public ActionResult Model([Bind(Prefix ="item1")]sporMerkezi1.Models.UyeKayit Model1,[Bind(Prefix ="item2")]sporMerkezi1.Models.Ders Model2)
            //{
        //    return View();
        //}


        // GET: Admin/KatilimListesi
        public ActionResult kListe(Ders ders)
        {

            if (ModelState.IsValid)
            {

                var Dersler = new List<Ders>
           {
               new Ders {DersID=200,DersTarihi=DateTime.Parse("19-12-2016"),DersSaati=DateTime.Parse("11:00:00"),DersAdi="Studio Pilates" },
                 new Ders {DersID=201,DersTarihi=DateTime.Parse("19-12-2016"),DersSaati=DateTime.Parse("15:00:00"),DersAdi="Spinning" },
                   new Ders {DersID=202,DersTarihi=DateTime.Parse("20-12-2016"),DersSaati=DateTime.Parse("12:00:00"),DersAdi="Zumba" },
                     new Ders {DersID=203,DersTarihi=DateTime.Parse("20-12-2016"),DersSaati=DateTime.Parse("16:00:00"),DersAdi="Grup Yüzme" },
                       new Ders {DersID=200,DersTarihi=DateTime.Parse("21-12-2016"),DersSaati=DateTime.Parse("11:00:00"),DersAdi="Studio Pilates" },
                       new Ders {DersID=201,DersTarihi=DateTime.Parse("21-12-2016"),DersSaati=DateTime.Parse("15:00:00"),DersAdi="Spinning" },
                         new Ders {DersID=200,DersTarihi=DateTime.Parse("22-12-2016"),DersSaati=DateTime.Parse("14:00:00"),DersAdi="Studio Pilates" },
                         new Ders {DersID=203,DersTarihi=DateTime.Parse("22-12-2016"),DersSaati=DateTime.Parse("18:00:00"),DersAdi="Grup Yüzme" },
                         new Ders {DersID=204,DersTarihi=DateTime.Parse("23-12-2016"),DersSaati=DateTime.Parse("11:00:00"),DersAdi="Abs&Strech" },
                         new Ders {DersID=205,DersTarihi=DateTime.Parse("23-12-2016"),DersSaati=DateTime.Parse("17:00:00"),DersAdi="Le Boxing" },
             };
                var dbContext = new sporContext();
                //s=> dbContext.Dersler.Add(s);
                dbContext.SaveChanges();

                var GrupKayitlar = new List<GrupKayit>
           {
               new GrupKayit {DersID=203,ID=1 },
               new GrupKayit {DersID=200,ID=1 },
               new GrupKayit {DersID=202,ID=2 },
               new GrupKayit {DersID=201,ID=3 }
           };

                //GrupKayitlar.ForEach(s => dbContext.GrupKayitlar.Add(s));
                dbContext.SaveChanges();
                

                return View(dbContext.Dersler.ToList());

            }
            else
            {
                return View();
            }
        }

        
      
    }
}

    
