﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using sporMerkezi1.Models;
using sporMerkezi1.Areas.Admin.Models;

namespace sporMerkezi1.DAL
{
    public class sporContext : DbContext
    {
        public sporContext() : base("sporContext")
        {

        }


        public DbSet<UyeKayit> UyeKayitlar { get; set; }
        public DbSet<GrupKayit> GrupKayitlar { get; set; }
        public DbSet<Ders> Dersler { get; set; }

        public DbSet<Yorum> Yorumlar { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }

    
}