﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sporMerkezi1.Models;
using sporMerkezi1.DAL;
using System.Web.Security;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Net;
using sporMerkezi1.Areas.Admin.Models;

namespace sporMerkezi1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Uyelikler()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        public ActionResult uyeKayit()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult uyeKayit(UyeKayit Kayit)
        {
            if (ModelState.IsValid)
            {
                var uye = new UyeKayit() { AdSoyad = Kayit.AdSoyad, TcNo = Kayit.TcNo, DogumTarihi = Kayit.DogumTarihi, EPosta = Kayit.EPosta, Telefon = Kayit.Telefon, KullaniciAdi = Kayit.KullaniciAdi, Sifre = Kayit.Sifre, YeniSifre = Kayit.YeniSifre };
                var dbContext = new sporContext();
                dbContext.UyeKayitlar.Add(uye);
                dbContext.SaveChanges();


                //ViewBag.Message = "Your contact page.";
                //veritabanına kaydet.
                return View("Onay", Kayit);
            }
            else
            {
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult uyeGiris()
        { 
            //ViewBag.Message = "Your contact page.";
            if (String.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                FormsAuthentication.SignOut();
                return View();
            }
            return RedirectToAction("Index","Home");
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult uyeGiris(Giris model, string returnurl)
        {
            if (ModelState.IsValid)
            {
                //var a = new Giris();
                var dbContext = new sporContext();
                var uye = dbContext.UyeKayitlar.Where(u => u.KullaniciAdi == model.KullaniciAdi && u.Sifre == model.Sifre).SingleOrDefault();
                RepositoryPortal<AdminUser> rpstryadmn = new RepositoryPortal<AdminUser>();
                //Aşağıdaki if komutu gönderilen mail ve şifre doğrultusunda kullanıcı kontrolu yapar. Eğer kullanıcı var ise login olur.
                if (uye != null)
                {
                    FormsAuthentication.SetAuthCookie(model.KullaniciAdi, true);
                    Session["uye"] = new AdminUser(model.KullaniciAdi);
                    return RedirectToAction("admindex", "Admin/AdminHome");
                }

                else
                {
                    ModelState.AddModelError("", "Kullanıcı Adı veya şifre hatalı!");
                }
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult uyeCikis()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "../Home");
        }
        
        [ChildActionOnly]
        public ActionResult  _Yorum()
        {
            var dbContext = new sporContext();
            var liste = dbContext.Yorumlar.OrderByDescending(x => x.YorumID);

            return View(liste);
        }
        
    }
}