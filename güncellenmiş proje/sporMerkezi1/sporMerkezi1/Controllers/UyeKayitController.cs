﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sporMerkezi1.DAL;
using sporMerkezi1.Models;

namespace sporMerkezi1.Controllers
{
    public class UyeKayitController : Controller
    {
        private sporContext db = new sporContext();

        // GET: UyeKayit
        public ActionResult Index()
        {
            return View(db.UyeKayitlar.ToList());
        }

        // GET: UyeKayit/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UyeKayit uyeKayit = db.UyeKayitlar.Find(id);
            if (uyeKayit == null)
            {
                return HttpNotFound();
            }
            return View(uyeKayit);
        }

        // GET: UyeKayit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UyeKayit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,AdSoyad,TcNo,DogumTarihi,Telefon,EPosta,KullaniciAdi,Sifre,YeniSifre")] UyeKayit uyeKayit)
        {
            if (ModelState.IsValid)
            {
                db.UyeKayitlar.Add(uyeKayit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uyeKayit);
        }

        // GET: UyeKayit/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UyeKayit uyeKayit = db.UyeKayitlar.Find(id);
            if (uyeKayit == null)
            {
                return HttpNotFound();
            }
            return View(uyeKayit);
        }

        // POST: UyeKayit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,AdSoyad,TcNo,DogumTarihi,Telefon,EPosta,KullaniciAdi,Sifre,YeniSifre")] UyeKayit uyeKayit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uyeKayit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uyeKayit);
        }

        // GET: UyeKayit/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UyeKayit uyeKayit = db.UyeKayitlar.Find(id);
            if (uyeKayit == null)
            {
                return HttpNotFound();
            }
            return View(uyeKayit);
        }

        // POST: UyeKayit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UyeKayit uyeKayit = db.UyeKayitlar.Find(id);
            db.UyeKayitlar.Remove(uyeKayit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
