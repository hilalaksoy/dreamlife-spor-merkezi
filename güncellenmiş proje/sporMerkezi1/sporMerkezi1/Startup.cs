﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sporMerkezi1.Startup))]
namespace sporMerkezi1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
